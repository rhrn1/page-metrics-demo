# Page metrics demo

### Live url
* https://metrics-demo.rhrn.ru/
* https://metrics-demo.rhrn.ru/view/page1

### Stack
  - [x] [Node.js](https://nodejs.org/) + [Babel](https://babeljs.io/)
  - [x] [Micro](https://github.com/zeit/micro) http framework
  - Queue
    - [ ] Kafka
    - [ ] RabbitMQ
    - [ ] Redis
  - Storage
    - [x] [Cockroachdb](https://www.cockroachlabs.com/) + [Sequelize](http://docs.sequelizejs.com/)
    - [ ] [Clickhouse](https://clickhouse.yandex/)
  - Cache
    - [x] Redis
  - [x] [Standard](https://standardjs.com/)
  - [x] [Jest](http://jestjs.io/)
  - [x] [Docker](https://www.docker.com/)

### Development local setup
* Requirements
  * docker
  * node.js 6 or higher

#### Install
```
npm install
```

#### Build from source
```
npm run build
```

#### Prepare Cockroach Database
```
docker network create dev_cockroach_net
docker-compose -f docker/cockroach.yml up -d
docker-compose -f docker/redis.yml up -d
docker exec -it docker_roach1_1 ./cockroach sql --insecure -e 'CREATE DATABASE metrics_test'
docker exec -it docker_roach1_1 ./cockroach sql --insecure -e 'CREATE DATABASE metrics_development'
npm run db:init:cockroach
NODE_ENV=test npm run db:init:cockroach
```

#### Docker build and run local
```
docker-compose -f docker/metrics.yml build
docker-compose -f docker/metrics.yml up
```

#### Test
```
npm test
```

#### Run
```
npm run dev
```

## Cockroach useful commands [statements](https://www.cockroachlabs.com/docs/v2.0/sql-statements.html)

* Cockroach sql console
```
docker exec -it docker_roach1_1 ./cockroach sql --insecure
```

* Show databases
```
docker exec -it docker_roach1_1 ./cockroach sql --insecure -e 'SHOW DATABASES'
```
