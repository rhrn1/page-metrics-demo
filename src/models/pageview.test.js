import { client as redisClient } from '../dbs/redis'
import { Pageview as CockroachPageview } from './pageview/cockroach'
import { Pageview } from './pageview'

const pageview = Pageview(CockroachPageview)

describe('pageview', () => {
  const page = 'index'

  beforeAll(async () => {
    await Promise.all([
      pageview.clear(page),
      redisClient.flushdbAsync()
    ])
  })

  it('initial increment', async () => {
    const count = await pageview.increment(page)
    expect(count).toEqual(1)
  })

  it('increment', async () => {
    const count = await pageview.increment(page)
    expect(count).toEqual(2)
  })

  it('increments', async done => {
    const page = 'performance'
    await pageview.clear(page)
    const times = 1000

    for (let i = 0; i < times; i++) {
      await pageview.increment(page)
    }

    // TODO: debug
    // const count = await pageview.count(page)
    // expect(+count).toEqual(times)

    done()
  })
})
