import { client as redisClient } from '../dbs/redis'
import { increment, get, set, del, exists, expire } from './cache'

describe('cache', () => {
  beforeAll(async () => {
    await redisClient.flushdbAsync()
  })

  it('get defaults', async () => {
    const key = 'test_key_get'
    const count = await get(key)
    expect(count).toEqual(null)
  })

  it('set ', async () => {
    const key = 'test_set_key'
    const value = '42'

    await set(key, value)

    const gettedValue = await get(key)
    expect(gettedValue).toEqual(value)
  })

  it('increment', async () => {
    const key = 'test_key_increment'

    const count = await increment(key)
    expect(count).toEqual(1)
  })

  it('increments', async () => {
    const key = 'test_key_increment2'
    await set(key, 0)
    const len = 10

    for (let i = 0; i < len; i++) {
      await increment(key)
    }

    const count = await get(key)
    expect(+count).toEqual(10)
  })

  it('exists', async () => {
    const key = 'test_key_exists'

    let isExists = await exists(key)
    expect(isExists).toBeFalsy()

    await set(key, 'exists')

    isExists = await exists(key)
    expect(isExists).toBeTruthy()
  })

  it('expire', async done => {
    const key = 'test_key_expire'
    let isExists = await exists(key)
    expect(isExists).toBeFalsy()

    await set(key, 'expire')
    await expire(key, 100)

    setTimeout(async () => {
      let isExists = await exists(key)
      expect(isExists).toBeTruthy()
    }, 50)

    setTimeout(async () => {
      let isExists = await exists(key)
      expect(isExists).toBeFalsy()
      done()
    }, 150)
  })

  it('set and expire', async done => {
    const key = 'test_key_set_end_expire'
    const value = 'setAndExpire'

    const getteValue = await get(key)
    expect(getteValue).toEqual(null)

    await set(key, value, 100)

    setTimeout(async () => {
      const getteValue = await get(key)
      expect(getteValue).toEqual(value)
    }, 50)

    setTimeout(async () => {
      const getteValue = await get(key)
      expect(getteValue).toEqual(null)
      done()
    }, 150)
  })

  it('delete', async () => {
    const key = 'test_key_delete'
    await set(key, 'delete')

    let isExists = await exists(key)
    expect(isExists).toBeTruthy()

    await del(key)

    isExists = await exists(key)
    expect(isExists).toBeFalsy()
  })
})
