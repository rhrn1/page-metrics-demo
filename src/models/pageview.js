import cfg from '../config'
import { increment, del, exists, set } from './cache'

export const PAGE_PREFIX = 'page:'

export function hashPage (page) {
  return Buffer.from(page).toString('base64')
}

export function pageKey (id) {
  return PAGE_PREFIX + id
}

export function Pageview (driver) {
  const model = { driver }

  model.incrementAsync = function (id) {
    setImmediate(() => {
      driver.incrementView(id)
    })
  }

  model.increment = async function (page) {
    const id = hashPage(page)
    const key = pageKey(id)
    const keyLock = key + '-lock'

    const hasCache = await exists(key)

    if (hasCache) {
      this.incrementAsync(id)
      return increment(key)
    }

    const hasCacheLock = await exists(keyLock)

    if (hasCacheLock) {
      this.incrementAsync(id)
      return null
    }

    let [ count ] = await Promise.all([
      this.driver.countViews(id),
      set(keyLock, 1000)
    ])

    this.incrementAsync(id)
    count++

    await Promise.all([
      set(key, count, cfg.pageviewsTTL),
      del(keyLock)
    ])

    return count
  }

  model.count = function (page) {
    const id = hashPage(page)
    return this.driver.countViews(id)
  }

  model.clear = function (page) {
    const id = hashPage(page)
    return this.driver.clearViews(id)
  }

  return model
}
