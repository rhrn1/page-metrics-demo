import { client } from '../dbs/redis'

export function get (key) {
  return client.getAsync(key)
}

export async function set (key, value, ms) {
  if (ms === undefined) {
    return client.setAsync(key, value)
  }
  await client.setAsync(key, value)
  return client.pexpireAsync(key, ms)
}

export function increment (key) {
  return client.incrAsync(key)
}

export function expire (key, ms) {
  return client.pexpireAsync(key, ms)
}

export function exists (key) {
  return client.existsAsync(key)
}

export function del (key) {
  return client.delAsync(key)
}
