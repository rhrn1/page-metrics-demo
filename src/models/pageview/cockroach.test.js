import { sequelize, Pageview } from './cockroach'

describe('view cockroach', () => {

  beforeAll(async () => {
    await Pageview.sync({ force: true })
  })

  afterAll(async () => {
    await sequelize.close()
  })

  it('view increment', async () => {
    await Pageview.incrementView('index')
    await Pageview.incrementView('index')
  })

  it('view count', async () => {
    const count = await Pageview.countViews('index')
    expect(count).toEqual(2)
  })
  
  it('view count random', async () => {
    const count = await Pageview.countViews('random')
    expect(count).toEqual(0)
  })

  it('view count timing', async done => {

    const page = 'timing'

    await Pageview.incrementView(page)
    setTimeout(async () => {
      await Pageview.incrementView(page)
      const count = await Pageview.countViews(page, 1000)
      expect(count).toEqual(1)
      done()
    }, 1000)
  })

  it('clear views', async () => {
    const page = 'index'
    let count = await Pageview.countViews(page, 999000)
    expect(count).toEqual(2)
    await Pageview.clearViews(page)
    count = await Pageview.countViews(page, 999000)
    expect(count).toEqual(0)
  })

})
