import Sequelize from 'sequelize-cockroachdb'
import { sequelize } from '../../dbs/cockroach'
import cfg from '../../config'
import { Op } from 'sequelize'
import uuid from 'uuid/v4'

const hooks = {
  beforeCreate: (instance) => {
    instance.id = uuid()
  }
}

const indexes = [ { fields: ['page'] } ]

const Pageview = sequelize.define('pageviews', {
  id: {
    type: Sequelize.UUID,
    defaultValue: Sequelize.UUIDV1,
    primaryKey: true
  },
  page: {
    type: Sequelize.STRING
  }
}, {
  hooks,
  indexes
})

Pageview.incrementView = function(page) {
  return this.create({ page })
}

Pageview.countViews = function(page, milliSecondsAgo = cfg.pageviewsTTL) {
  const where = {
    page,
    createdAt: {
      [Op.gte]: (new Date() - milliSecondsAgo)
    }
  }
  return this.count({ where })
}

Pageview.clearViews = function(page) {
  const where = {
    page
  }
  return this.destroy({ where })
}

export { sequelize, Pageview }
