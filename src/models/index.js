import { Pageview as CockroachPageview } from './pageview/cockroach'
import { Pageview } from './pageview'

const pageview = Pageview(CockroachPageview)

export { pageview }
