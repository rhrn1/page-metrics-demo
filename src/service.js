import micro from 'micro'
import Router from 'micro-http-router'
import { pageview } from './models'
import { version } from '../package'

const router = new Router()

router.route({
  path: '/',
  method: 'GET',
  handler: async (req, res) => {
    micro.send(res, 200, {
      version,
      endpoints: [
        {
          path: '/view/:page',
          description: 'Increment page view'
        }
      ]
    })
  }
})

router.route({
  path: '/view/:page',
  method: 'GET',
  handler: async (req, res) => {
    const { params } = req
    let result
    try {
      result = await pageview.increment(params.page)
    } catch (e) {
      console.log(e)
      throw e
    }
    micro.send(res, 200, {
      page: params.page,
      pageviews: result
    })
  }
})

const service = micro((req, res) => router.handle(req, res))

export { service }
