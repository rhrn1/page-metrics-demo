import cfg from './config'

describe('config', () => {

  it('merge', () => {
    expect(cfg.name).toEqual('Test')
    expect(cfg.env).toEqual('test')
  })

})
