import { service } from './service'

const port = process.env.PORT || 3000

service.listen(port)

console.log(`Listening on port: ${ port }`)
