import { client as redisClient } from './dbs/redis'
import listen from 'test-listen'
import request from 'request-promise'
import { service } from './service'
import { pageview } from './models'
import { version } from '../package'

describe('micro service', () => {

  let url
  const page = 'test-page'

  beforeAll(async () => {
    url = await listen(service)
    await pageview.clear(page)
    await redisClient.flushdbAsync()
  })

  afterAll(async () => {
    await service.close()
  })

  it('pageview index', async () => {

    const response = await request({
      uri: `${ url }/`,
      method: 'GET'
    })

    const res = JSON.parse(response)
    expect(res.version).toEqual(version)
  })

  it('increment pageview', async () => {
    const response = await request({
      uri: `${ url }/view/${ page }`,
      method: 'GET'
    })

    const res = JSON.parse(response)
    expect(res.page).toEqual(page)
    expect(res.pageviews).toEqual(1)
  })

  it('second increment pageview', async () => {
    const response = await request({
      uri: `${ url }/view/${ page }`,
      method: 'GET'
    })

    const res = JSON.parse(response)
    expect(res.page).toEqual(page)
    expect(res.pageviews).toEqual(2)
  })

})
