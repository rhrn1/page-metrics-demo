import { defaultsDeep } from 'lodash'

const env = process.env.NODE_ENV || 'development'

const configs = {

  defaults: {
    name: 'Defaults',
    env,
    pageviewsTTL: 60000
  },

  test: {
    name: 'Test',
    pageviewsTTL: 500,
    redis: {
      db: 4
    },
    cockroach: {
      port: 26257,
      username: 'root',
      database: 'metrics_test',
    },
  },

  development: {
    name: 'Development',
    redis: {
      db: 5
    },
    cockroach: {
      port: 26257,
      username: 'root',
      database: 'metrics_development',
    },
  },

  staging: {
    name: 'Staging',
    redis: {
      db: 6
    },
    cockroach: {
      port: 26257,
      username: 'root',
      database: 'metrics_staging',
    },
  },

  production: {
    name: 'Production',
    redis: {
      db: 9
    },
    cockroach: {
      port: 26257,
      username: 'root',
      database: 'metrics_production',
    },
  }

}

const config = defaultsDeep(configs[env], configs.defaults)

export default config
