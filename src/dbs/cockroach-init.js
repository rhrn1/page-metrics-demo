import { sequelize } from '../models/pageview/cockroach'

export async function sync () {
  try {
    await sequelize.sync()
    process.exit(0)
  } catch (e) {
    console.log(e)
    process.exit(1)
  }
}

sync()
