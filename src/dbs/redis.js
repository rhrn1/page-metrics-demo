import redis from 'redis'
import { promisify } from 'util'
import cfg from '../config'

const connect = { host: process.env.REDIS_HOST || 'localhost' }

const client = redis.createClient(connect)
client.select(cfg.redis.db, err => {
  if (err) {
    throw err
  }
  console.log('Redis selected', cfg.redis.db)
})

client.getAsync = promisify(client.get)
client.setAsync = promisify(client.set)
client.incrAsync = promisify(client.incr)
client.pexpireAsync = promisify(client.pexpire)
client.existsAsync = promisify(client.exists)
client.flushdbAsync = promisify(client.flushdb)
client.delAsync = promisify(client.del)

export { client }
export default client
