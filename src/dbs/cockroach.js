import Sequelize from 'sequelize-cockroachdb'
import cfg from '../config'

const sequelize = new Sequelize(cfg.cockroach.database, cfg.cockroach.username, '', {
  dialect: 'postgres',
  host: process.env.COCKROACHDB_HOST || 'localhost',
  port: cfg.cockroach.port,
  logging: false
})

export {
  Sequelize,
  sequelize
}
